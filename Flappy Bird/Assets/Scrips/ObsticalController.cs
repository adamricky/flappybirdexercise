﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsticalController : MonoBehaviour {

	public Transform topPiller;
	public Transform bottomPiller;


	public void setup(float gapSpace){
		topPiller.position = new Vector2 (topPiller.position.x, topPiller.position.y + gapSpace/2);
		bottomPiller.position = new Vector2 (bottomPiller.position.x, bottomPiller.position.y - gapSpace/2);
	}
}
