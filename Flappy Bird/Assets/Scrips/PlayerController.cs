﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private Rigidbody2D rb;
	public Vector2 forceScale;

	private GameController gameController;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();

		gameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			rb.velocity = new Vector2 (0f, Random.Range(forceScale.x, forceScale.y));
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Obstical")) {
			gameOver ();
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.CompareTag ("Boundary")) {
			gameOver ();
		}
	}

	void gameOver(){
		gameController.GameOver ();
		Destroy (gameObject);
	}
}
