﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	public GameObject obstical;
	public float spawnRate;
	public Vector2 yRange;
	public Vector2 gapRange;

	public Text scoreText;
	private int currentScore;
	public float scoreStartWaitTime;

	public Text gameOverText;

	private bool gameOver;
	private bool restart;

	public Text restartText;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (spawnObstacles ());
		currentScore = 0;
		IncreaseScore (0);

		StartCoroutine (IncrementScore ());
		gameOver = false;
		restart = false;

		gameOverText.enabled = false;
		restartText.enabled = false;
	}

	IEnumerator IncrementScore(){
		yield return new WaitForSeconds (scoreStartWaitTime);
		while (true) {
			if (gameOver) {
				break;
			}

			IncreaseScore (1);
			yield return new WaitForSeconds (spawnRate);
		}

	}

	IEnumerator spawnObstacles ()
	{
		while (true) {
			Vector2 position = new Vector2 (12, 0);
			GameObject newObstcale = Instantiate (obstical, position, Quaternion.identity) as GameObject;
			ObsticalController obsController = newObstcale.GetComponent<ObsticalController> ();
			obsController.setup (Random.Range (gapRange.x, gapRange.y));

			newObstcale.transform.position = new Vector2 (12, Random.Range (yRange.x, yRange.y));

			yield return new WaitForSeconds (spawnRate);

			if (gameOver) {
				break;
			}
		}
	}

	public void IncreaseScore(int amount){
		currentScore += amount;
		scoreText.text = "Score: " + currentScore;
	}

	public void GameOver(){
		gameOver = true;
		gameOverText.enabled = true;
		restart = true;
		restartText.enabled = true;
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.R) && restart) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}

	}
}
